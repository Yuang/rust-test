#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

#[get("/")]
fn index() -> &'static str {
        "Hello, world!"
}

use std::thread;
fn server_run() {
        rocket::ignite().mount("/", routes![index]).launch();
}

fn main() {
    let t1 = thread::spawn(server_run);
    let t2 = thread::spawn(server_run);
    t1.join();
    t2.join();
    return ;
}
