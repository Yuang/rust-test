#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
    use sqlite::open;
    #[test]
    fn sqlite1(){
        let mut connection = sqlite::open(":memory:").unwrap();
        connection
            .execute("
                    drop if exist users;
            ");
        connection
            .execute("
                create table users(name Text,age Integer);
                insert INTO users  (name, age) values ('alice',42);
                insert into users  (name, age) values ('Bob',69);
                insert into users  (name, age) values ('Bo3',69);
            ");
        connection.iterate("select * from users where age > 50",|pairs|{
            for &(column,value) in pairs.iter(){
                println!("{} = {}",column,value.unwrap());
            } true } );
    }
    #[test]
    fn block_save_and_fetch(){
        println!("Test block_save_and_fetch.");
        let mut connection = sqlite::open(":memory:").unwrap();
        let res_in_create_table = connection
            .execute("
                create table blocks(hash_id Text, value Text);
                insert into blocks (hash_id, value) values ('123','wertyuikmnbftyujnbvfghj');
                insert into blocks (hash_id, value) values ('124','wertyuikmnbftyujnbvfghj');
            ");
        match &res_in_create_table {
            Ok(T)=>{println!("Success in creating table.");},
            Err(E)=>{println!("Err in creating table.{}",E);}
        }
        connection
            .execute("insert into blocks (hash_id, value) values ('124','wertyuikmnbftyujnbvfghj');").expect("123");
        connection
            .execute("insert into blocks (hash_id, value) values ('125','wertyuikmnbftyujnbvfghj');").expect("123");
        println!("---");
        connection.iterate("
            select * from blocks
        ",|pairs|{
            for &(column,value) in pairs.iter(){
                println!("block_id:{}, content:{}",column,value.unwrap());
            } true
        }).expect("fail in print result!");
        println!("Test end");
    }

    // 这个测试分成以下几部分.首先是一个简单的struct,即block.
    // 随后，这玩意有若干个impl,可以
    pub struct Block{
        hash_id:String,
        value:String,
    }
    pub fn create_database(){
        let connection = open("sqlite.db").expect("fail in opening database");
        connection.execute("drop table if exists blocks").expect("Error in droping table");
        connection.execute("create table if not exists blocks (hash_id Text, value Text);").expect("fail in creating table"); 
    }

    pub fn insert(hash_id:&String,value:&String){
        let mut connection = open("sqlite.db").expect("fail in opening connection");
        let command = format!("insert into blocks (hash_id,value) values ('{}', '{}');",hash_id,value);
        println!("{}",command);
        connection.execute(&command).expect("fail in inserting ");
    }

    pub fn insert_block(block:&Block){
        insert(&block.hash_id,&block.value);
    }
    
    fn fetch(){
        let mut connection = open("sqlite.db").expect("fail in opening database");
        connection.iterate(
            "select * from blocks;",
            |pairs|{
                for &(column,value) in pairs.iter(){
                    print!("{} = {}\t",column,value.unwrap());
                };
                println!("");
                true
            } 
        );
    }

    #[test]
    fn simple_try(){
        create_database();
        insert(&"123".into(),&"324".into());
        insert(&"124".into(),&"321".into());
        insert(&"125".into(),&"322".into());
        insert(&"126".into(),&"323".into());
        fetch();
    }

    #[test]
    fn to_vec_and_insert(){
        let target = vec![
            Block{
                hash_id:"1".into(),
                value:"value".into()
            },
            Block{
                hash_id:"2".into(),
                value:"value2".into()
            }
        ];
        create_database();
        for block in target.iter(){
            insert_block(block);
        }
        fetch()
    }
}

