use yuang_time;
use tool;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

// --------------------------------------------------------------------------
// trait hash_obj
pub trait HashObj{
    fn hash(&mut self)->&String;
    fn freeze(&mut self);
    fn set_time(&mut self,time:u64);
}


// --------------------------------------------------------------------------
// 数据块,记录同一数据源的一条数据
pub struct DataPiece{
    block_hash: String,
    device_id: String,
    timestamp: u64, // 时间戳,即数据对应的时间.
    content: String, // 内容,即实际数据.
}

impl DataPiece{
    //pub fn new_piece()->DataPiece{
    //}
    pub fn new_piece(content:String)->DataPiece{
        DataPiece{
            block_hash:"".to_string(),
            device_id:"".to_string(),
            timestamp: 0,
            content: content
        }
    }
}

impl HashObj for DataPiece{
    fn hash(&mut self)->&String{
        &self.block_hash
    }
    fn set_time(&mut self,time:u64){
        self.timestamp = time;
    }
    fn freeze(&mut self){
        self.set_time(yuang_time::get_timestamp());
    }
}

// ---------------------------------------------------------------------------
// 数据链,记录同一数据源的多条数据.
pub struct DataChain{
    device_id: String,
    block_hash: String,
    timestamp: u64,      // 时间戳,这里指的是,计算block_hash的时间.,
    DataPieces: Vec<DataPiece>,  // 
}

impl DataChain{
    fn add_piece(&mut self,piece:DataPiece){
        self.DataPieces.push(piece);
    }
}

impl HashObj for DataChain{
    fn hash(&mut self)->&String{
        &self.block_hash
    }
    fn set_time(&mut self,time:u64){}
    fn freeze(&mut self){}
}

// --------------------------------------------------------------------------
struct Datas{
    root_hash: String
}

impl HashObj for Datas{
    fn hash(&mut self)->&String{
        &self.root_hash
    }
    fn set_time(&mut self,time:u64){}
    fn freeze(&mut self){}
}
// --------------------------------------------------------------------------
//数据块的影子,仅包含哈希值等少数数据.
pub struct BlockShadow{
    block_hash: String
}

// --------------------------------------------------------------------------

pub struct DataBlock{
    block_hash: String,
    device_uuid: String,
    timestamp: u64,
    parent_block: BlockShadow,
    block_ref_data: Vec<BlockShadow>,
    block_ref_struct: Vec<BlockShadow>,
    datas: Datas,
}


impl DataBlock{
    pub fn data_block_blank()->DataBlock{
        DataBlock{
            block_hash: "".to_string(),
            device_uuid: "".to_string(),
            timestamp: 0,
            parent_block: BlockShadow{
                block_hash: "".to_string(),
            },
            block_ref_data: vec![],
            block_ref_struct: vec![],
            datas: Datas{
                root_hash:"".to_string()
            }
        }
    }
    pub fn set_parent_block(&mut self,block:BlockShadow){
        self.parent_block = block;
    }
    pub fn add_related_data_block(&mut self,data_block:BlockShadow){
        self.block_ref_data.push(data_block);
    }
    pub fn pop_datablock_by_hash(&mut self,hash:String){}
    pub fn add_related_struct_block(&mut self,struct_block:BlockShadow){
        self.block_ref_struct.push(struct_block);
    }
    pub fn pop_structblock_by_hash(&mut self,hash:String){}
    pub fn to_text(self)->String{
        let res = "".to_string();
        res
    }
}

impl HashObj for DataBlock{
    fn hash(&mut self)->&String{
        &self.block_hash
    }
    fn freeze(&mut self){
        self.set_time(yuang_time::get_timestamp());
    }
    fn set_time(&mut self,time:u64){
        self.timestamp = time;
    }
}

#[test]
fn freeze_block(){
    let mut block = DataBlock::data_block_blank();
    block.freeze();
    println!("[block_time:{:?}]",block.timestamp);
}
