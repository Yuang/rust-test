#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}


#[cfg(test)]
mod test_1{
    use std::time::SystemTime;
    #[test]
    fn systime(){
        let systime = SystemTime::now();
        println!("[system:{:?}]",systime);
    }

    #[test]
    fn try_to_get_timestamp(){
        match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH){
            Ok(n)=>println!("1970-01-01 00:00:00 UTC was {} seconds ago!",n.as_secs()),
            Err(_)=>panic!("SystemTime before Unix Epoch!"),
        }
    }
}


use std::time::SystemTime;
pub fn get_timestamp()->u64{
    match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH){
        Ok(n)=>return n.as_secs(),
        Err(_)=>panic!("SystemTime before UNIX_EPOCH!"),
    }
}
