use std::thread;
use std::time::Duration;

fn thread_fn(){
    for i in 0..10{
        println!("Hi number {} from the spawned thread!",i);
        thread::sleep(Duration::from_secs(1));
    }
}


fn main() {
    println!("Hello, world!");
    let t = thread::spawn(thread_fn);
    t.join();
    return ;
}
