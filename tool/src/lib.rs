#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

pub fn int2hex(&value:&u8)->char{
    "0123456789abcdef".to_string().chars().nth(value as usize).expect("fail in int 2 hex")
}

pub fn v8_to_string(content:&Vec<u8>)->String{
    let mut res = String::new();
    for u in content{
        let hchar = u.clone()>>4;
        let lchar = u.clone() & 0b00001111u8;
        res.push(int2hex(&hchar));
        res.push(int2hex(&lchar));
    }
    res
}


#[test]
fn v8_to_string_test(){
    let v = vec![166,101];
    let s:String = v8_to_string(&v);
    assert_eq!(s,"a665".to_string());
}
