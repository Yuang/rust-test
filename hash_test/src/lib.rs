#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
//
//    use blake2::{Blake2b, Digest};
//    #[test]
//    fn test_hash(){
//        let mut hasher = Blake2b::new();
//        let data = b"Hello world!";
//        hasher.update(data);
//        hasher.update("String data");
//        let hash = hasher.finalize();
//        println!("Result: {:x}",hash);
//    }
}


mod struct_mode_test{
    use blake2::{Blake2b,Digest};
    use generic_array::GenericArray;
    struct TestStruct{
        value:i64,
    }
    
    impl TestStruct{
        pub fn hash(&mut self)->[u8;64]{
            let mut buffer = [0u8;64];
            let value = Blake2b::new()
            .chain(self.value.to_string())
            .finalize();
            println!("len {:?}",value.as_slice().len());
            buffer.copy_from_slice(&value);
            println!("value\t\t:{:?}",value);
            println!("raw result\t:{:?}",buffer);
            println!("buffer result\t:{:?}",Vec::from(buffer));
            //println!("str\t\t:{:?}",str::from(buffer));
            println!("to string:{:?}",String::from_utf8(Vec::from(buffer)));
            buffer
        }
        pub fn low8u(&mut self)->u8{
            1u8
        }
    }

    #[test]
    fn calculate_struct_hash(){
        let target = TestStruct{
            value: 2,
        };
        let value = Blake2b::new().chain(target.value.to_string()).finalize();
        println!("Result 直接计算hash,:x: {:x}",value);
        println!("Result 直接计算hash,:?: {:?}",value);
    }
    #[test]
    fn calculate_struct_self_hasher(){
        let mut target = TestStruct{
            value:2 ,
        };
        let mut buffer = String::new();
        for c in target.hash().iter(){
            // println!("char:{}",c);
            buffer.push(c.clone() as char)
        }
        // println!("Result of self.hash(): {:?}",buffer);
    }

    
}


mod learn_generic_array{
    use generic_array::{{GenericArray,ArrayLength}};
    use generic_array::typenum::U5;
    struct Foo<T,N:ArrayLength<T>>{
        data: GenericArray<T,N>
    }
    fn basic(){
        let foo = Foo::<i32,U5>{data:GenericArray::default()};
    }
}

mod learn_rsa_lib{
    extern crate rsa;
    extern crate crypto;
    
    use crypto::sha3::Sha3;
    use crypto::sha2::Sha256;
    use crypto::digest::Digest;
    use std::iter::repeat;

    use rsa::{RSAPrivateKey,PaddingScheme};
    use rsa::Hash;

    pub enum HashType{
        Sha1,
        Sha256
    }


    pub fn rsa_sigh(content:&str,private_key:&str,hash_type:HashType)->String{
        let der_encoded = private_key.lines()
            .filter(|line|!line.starts_with("-"))
            .fold(String::new(),|mut data,line|{
                data.push_str(&line);
                data
            });
        println!("der_encoded:{:?}\n-----------------------------------",der_encoded);
        let der_bytes = base64::decode(der_encoded).expect("failed to decode base64 content");
        println!("der_bytes:{:?}\n-----------------------------------",der_bytes);
        let private_key = RSAPrivateKey::from_pkcs1(&der_bytes).expect("fail to parse key");
        println!("private_ket:{:?}\n-----------------------------------",private_key);
        let mut hasher = Sha256::new();
        hasher.input_str(content);
        println!("hasher.output:{}\n-----------------------------------",hasher.output_bits());
        let mut buf:Vec<u8> = repeat(0).take((hasher.output_bits()+7)/8).collect();
        println!("buf:{:?}\n-----------------------------------",buf);
        hasher.result(&mut buf);
        println!("buf:{:?}\n-----------------------------------",buf);

        let hash; 
        match hash_type{
            HashType::Sha1 => hash = Hash::SHA1,
            HashType::Sha256 => hash = Hash::SHA2_256
        }
        let sign_result = private_key.sign(PaddingScheme::PKCS1v15Sign{hash: Option::from(hash)},&buf);
        let vec = sign_result.expect("create sign error for base64");

        base64::encode(vec)
    }

    #[test]
    fn rsa_works(){
        let content = "123";
        let private_key = "MIICXAIBAAKBgQCHJlAPN+1dCbgc3HiahQkT2W/skecGWOCkSX4CPvEc8oIk6544\nxihEwShHnfrapiQdF2fndv5agrhg4FyOHheST42L5MnCk+4Km+mWm5GDvmFS7Sa2\naZ5o3regY0MUoJ7D74dYjE3UYFuTujAXiXjGpAwa9qOcKotov5LCkSfUeQIDAQAB\nAoGAB1cyw8LYRQSHQCUO9Wiaq730jPNHSrJW4EGAIz/XMYjv/fCgx0lnDEX4CbzI\nUGoz/bME4R721YRyXoutJ0h14/cGrt/TEn/TMI0xnISzJHr8VSlyBkQEdfO/W3LO\nqjs/UYq2Bz4+kJROJHreM+7d5hiIWLzLBlyI8cSU92ySmHECQQDwju2SoRu88kQP\n1qr4seZyKQa8DHTVyCoa6LtPLXyJsdgWgY4KyqJHwMUumEC2Zhhu833CR0ZXbfta\nuQDmwAVJAkEAj9M225jrPasaD5vPO7thxtEqgV4F/ZyNKH0Z8bDH27KaKkQ+8GMt\nkxwKVckZXs2bMvg/6tCiDZkWAxawNrvFsQJBANmTrPWOmpQPW9gnhYRjA9gFm33C\nlno2DT9BeQloTtgL7zKMA3lnRdg4VyCJvR48waS4vupVpR228D1iT5pl22ECQF1M\nJUzkcM0rPheb+h2EW1QOgWU0Keyvbj4ykO7gv3T78dezN6TWoUzJpsapUiTWeXPh\n6AyZ1FW/1bChOiP3QLECQGAbObmsYlN0bjzPYChwWYeYjErXuv51a44GZCNWinFw\nGGiHU9ZAqF8RzmBVW4htwj0j/Yry/V1Sp0uoP0zu3uA=";
        let sign = rsa_sigh(content,private_key,HashType::Sha256);
        println!("len:{}\nsign:{}",sign.len(),sign);
    }

    #[test]
    fn rsa_basic(){
        let mut hasher = Sha256::new();
        hasher.input_str("123");
        let mut buffer:Vec<u8> = repeat(0).take((hasher.output_bits()+7)/8).collect();
        hasher.result(&mut buffer);
        println!("-------------------------------\nbuffer:{:?}\n----------------------------------",buffer);
    }
    #[test]
    fn rsa_string_hash(){
        let mut hasher = Sha256::new();
        hasher.input_str("123".to_string().as_str());
        let mut buffer:Vec<u8> = repeat(0).take((hasher.output_bits()+7)/8).collect();
        hasher.result(&mut buffer);
        println!("-------------------------------\nbuffer:{:?}\n----------------------------------",buffer);
    }
}


mod hash_struct{
    extern crate rsa;
    extern crate crypto;

    use crypto::sha2::Sha256;
    use crypto::digest::Digest;
    use std::iter::repeat;
    use std::collections::HashMap;

    fn int2hex(&value:&u8)->char{
        let map:String = "0123456789abcdef".to_string();
        map.chars().nth(value as usize).expect("fail in transfering int to char")
    }

    #[test]
    fn int2hex_test(){
        for i in 0..16{
            println!("raw:{}=>char:{}",i,int2hex(&i));
        }
    }

    fn Vecu8toString(content:&Vec<u8>)->String{
        let len = &content.len();
        let mut res = String::new();
        for u in content{
            let highchar = u.clone()>>4;
            let lowchar = u.clone() & 0b00001111u8; 
            res.push(int2hex(&highchar));
            res.push(int2hex(&lowchar));
            //println!("u8:{}, higher:{:b}|{:x}, lower:{:b}|{:x}",u,highchar,highchar,lowchar,lowchar);
        }
        res
    }

    #[test]
    fn print_test(){
        Vecu8toString(&vec![166u8,101u8]);
    }

    trait HashObj{
        fn hash(&self)->Vec<u8>{
            vec![0u8]
        }
        fn hash_into(&mut self){
        }
        fn hash_gen(&mut self){

        }
    }

    struct main{
        sub1: SObj,
        sub2: SObj2,
        stamp: String

    }
    impl HashObj for main{
        fn hash(&self)->Vec<u8>{
            let mut hasher = Sha256::new();
            hasher.input_str(Vecu8toString(&self.sub1.hash()).as_str());
            hasher.input_str(Vecu8toString(&self.sub2.hash()).as_str());
            hasher.input_str(self.stamp.as_str());
            let mut buffer:Vec<u8> = repeat(0).take((hasher.output_bits()+7)/8).collect();
            hasher.result(&mut buffer);
            buffer
        }

    }
    struct SObj{
        value:String
    }
    impl HashObj for SObj{
        fn hash(&self)->Vec<u8>{
            let mut hasher = Sha256::new();      
            hasher.input_str(self.value.as_str());
            let mut buffer:Vec<u8> = repeat(0).take((hasher.output_bits()+7)/8).collect();
            hasher.result(&mut buffer);
            println!("{:?}",buffer);
            buffer
        }
    }

    struct SObj2{
        value: String,
    }
    impl HashObj for SObj2{
        fn hash(&self)->Vec<u8>{
            let mut hasher = Sha256::new();
            hasher.input_str(self.value.as_str());
            let mut buffer:Vec<u8> = repeat(0).take((hasher.output_bits()+7)/8).collect();
            hasher.result(&mut buffer);
            buffer
        }

    }

    #[test]
    fn SObj_1_test(){
        let tmp = SObj{
            value:"123".to_string()
        };
        let hash_value = tmp.hash();
        println!("{:?}",hash_value);
    }

    #[test]
    fn SObj_2_test(){
        let tmp = SObj2{
            value:"123".to_string()
        };
        let hash_value = Vecu8toString(&tmp.hash());
        println!("main hash value String :{:?}",hash_value);
    }

    #[test]
    fn main_struct_test(){
        let tmp = main{
            sub1:SObj{
                value:"123".to_string()
            },
            sub2:SObj2{
                value:"123".to_string()
            },
            stamp:"123".to_string()
        };
        let hash_value = Vecu8toString(&tmp.hash());
        println!("main hash value String :{:?}",hash_value);
    }

    struct TreeNode{
        left:Option<Box<TreeNode>>,
        right:Option<Box<TreeNode>>,
        value:String
    }
    
    impl TreeNode{
        fn new()->TreeNode{
            TreeNode{
                left:None,
                right:None,
                value:String::new()
            }
        }
        fn new_with_value(value:String)->TreeNode{
            TreeNode{
                left:None,
                right:None,
                value:value
            }
        }
        fn insert_left(&mut self,value:String){
            self.left = Some(Box::new(TreeNode::new_with_value(value)));
        }
        fn insert_right(&mut self,value:String){
            self.right = Some(Box::new(TreeNode::new_with_value(value)));
        }
    }
    impl HashObj for TreeNode{
        fn hash(&self)->Vec<u8>{
            let mut hasher = Sha256::new();
            hasher.input_str(self.value.as_str());
            match &self.left{
                Some(v)=>{
                    hasher.input_str(Vecu8toString(&v.hash()).as_str());
                }
                None=>{}
            }
            //if self.right.is_some(){
            //    hasher.input_str(Vecu8toString(&(&self.right.unwrap()).hash()).as_str());
            //}
            match &self.right{
                Some(v)=>{
                    hasher.input_str(Vecu8toString(&v.hash()).as_str());
                }
                None=>{}
            }
            let mut buffer:Vec<u8> = repeat(0).take((hasher.output_bits()+7)/8).collect();
            hasher.result(&mut buffer);
            buffer
        }
    }

    #[test]
    fn TreeHashTest(){
        let mut tree = TreeNode::new_with_value("123".to_string());
        println!("Tree Hash:{}",Vecu8toString(&tree.hash()));
        let mut tree2 = TreeNode::new_with_value("123".to_string());
        println!("Tree2 Hash:{}",Vecu8toString(&tree2.hash()));
        tree2.insert_left("left".to_string());
        println!("Tree2 Hash:{}",Vecu8toString(&tree2.hash()));
        tree2.insert_right("right".to_string());
        println!("Tree2 Hash:{}",Vecu8toString(&tree2.hash()));
        match tree2.left{
            Some(ref mut left)=>{left.insert_left("deep left".to_string());}
            None=>{}
        }
        println!("Tree2 Hash:{}",Vecu8toString(&tree2.hash()));
        //let new_tree:&mut Box<TreeNode> = &mut tree2.left.unwrap();
        //println!("Tree2 Hash:{}",Vecu8toString(&tree2.hash()));

    }


    struct List{
        values:Vec<SObj>
    }
    
    impl HashObj for List{
        fn hash(&self)->Vec<u8>{
            let mut hasher = Sha256::new();
            for sobj in &self.values{
                hasher.input_str(Vecu8toString(&sobj.hash()).as_str());
            }
            let mut buffer:Vec<u8> = repeat(0u8).take(hasher.output_bytes()).collect();
            hasher.result(&mut buffer);
            buffer
        }
    }

    #[test]
    fn ListHashTest(){
        let list = List{
            values:vec![]
        };
        let hash_value = Vecu8toString(&list.hash());
        println!("List hash value:{}",hash_value);
    }
}


